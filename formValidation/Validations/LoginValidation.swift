//
//  LoginValidation.swift
//  formValidation
//
//  Created by Dev on 10/30/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import Foundation
import SwiftValidator

class LoginValidation: ValidationDelegate {
    func validationSuccessful() {
        
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    static func emailValidation() -> Array<Rule> {
        return [
            RequiredRule(message: "El email es requerido"),
            EmailRule(message: "El email es invalido"),
            MinLengthRule(length: 10, message: "Parece que es muy corto para cer un email valido"),
            MaxLengthRule(length: 30, message: "El email posee demaciados caracteres")
        ]
    }
    
    static func passwdValidation() -> Array<Rule> {
        return [
            RequiredRule(message: "El passwd es requerido"),
            MinLengthRule(length: 5, message: "Debe de contener mas de 5 caracteres"),
            MaxLengthRule(length: 15, message: "No debe de superar los 15 caracteres")
        ]
    }
}
