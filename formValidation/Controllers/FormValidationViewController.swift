//
//  FormValidationViewController.swift
//  formValidation
//
//  Created by Dev on 10/29/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit
import SwiftValidator

class FormValidationViewController: UIViewController, ValidationDelegate, UITextFieldDelegate {


    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwdField: UITextField!
    
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwdErrorLabel: UILabel!
    
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailErrorLabel.isHidden = true
        passwdErrorLabel.isHidden = true
        
        validator.styleTransformers(success: { (validationRule) -> Void in
            
            validationRule.errorLabel?.isHidden = true
            validationRule.errorLabel?.text = ""
            if let textField = validationRule.field as? UITextField {
                textField.layer.borderColor = UIColor.green.cgColor
                textField.layer.borderWidth = 0.5
                
            }
            
        }, error: { (validationError) -> Void in
            
            validationError.errorLabel?.isHidden = false
            validationError.errorLabel?.text = validationError.errorMessage
            if let textField = validationError.field as? UITextField {
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 1.0
            }
        })
        
        validator.registerField(emailField, errorLabel: emailErrorLabel,  rules: LoginValidation.emailValidation())
        
        validator.registerField(passwdField, errorLabel: passwdErrorLabel, rules: LoginValidation.passwdValidation())
    }
    
    
    @IBAction func signin(_ sender: UIButton) {
        validator.validate(self)
    }
    
    func validationSuccessful() {
        print("form ok")
    }
    

    
    func validationFailed(_ errors:[(Validatable, ValidationError)]) {
        
        for (field, error) in errors {
            
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
                
                error.errorLabel?.text = error.errorMessage
                error.errorLabel?.isHidden = false
                print("Field: \(field)")
                print("error, \(error)")
            } else {
                error.errorLabel?.isHidden = true
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validator.validateField(textField){ error in
            if error == nil {
                
            } else {
                
            }
        }
        return true
    }
    

    
    @IBAction func when(_ sender: UITextField) {
        print("blabla")
       // validator.validate(self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
